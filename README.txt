
-- SUMMARY --

The PersonalTube Video Widget lets you display the latest Internet videos that
precisely match the topic and theme of your web site, and attract longer, more
frequent visits from your audience.

When you [sign up with PersonalTube]
(http://www.personaltube.com/publisher/signup), you get your own video feed
widget that you can easily personalize, using a password-protected account on
our site.

PersonalTube provides you with exceptional control over the video content,
appearance and branding of your widget.
You can personalize the video widget to display the exact kind of videos you
want for your web site, using keywords as well as topics.
Our world leading collection of over 300,000 video topics, organized in an
intuitive, browsable hierarchy, is wide enough and deep enough to describe
almost any concept or subject you select. For precise control, you can also
specify the exact videos that should be displayed.

You can also control the colors and size of the video widget to blend in with
your web site theme, and personalize the video playback display with your own
branding and backlinks.

Once the widget is installed and configured, our servers continuously crawl the
Internet, retrieve and rank the latest videos precisely matching your
requirements, and automatically display them on your widget.

Engage your visitors, attract return visits and expand your audience and
traffic, with a PersonalTube personalized video widget.

To learn more, visit [our drupal page]
(http://www.personaltube.com/publisher/install/drupal_video_widget).
To sign up for a PersonalTube Drupal widget, visit [our signup page]
(http://www.personaltube.com/publisher/signup).


-- INSTALLATION --

* Download the PersonalTube module zip file from
  http://drupal.org/project/Modules and drop the contents in the
  sites/all/modules/ directory of your Drupal installation.

* See http://drupal.org/node/70151 for further information.

* Now you must configure the widget with the Configuration String from 
  PersonalTube. This requires you to [sign up for a PersonalTube account]
  (http://www.personaltube.com/publisher/signup).

* After signing up, login to your [PersonalTube account]
  (http://www.personaltube.com/publisher/dashboard).

* Personalize the widget video content, appearance and branding, following the
  instructions on your PersonalTube account.

* Configure the widget size to fit in at the desired location in your web site.

* Once you are satisfied with your personalization, select the 'Install Widget'
  option on the left column of your PersonalTube account page.

* Please ensure that you have entered the exact URL of your Web page in the
  'Web Page URLs' section.

* Choose the Drupal Web Page option under the 'Installation Options' section.

* Look for the Drupal Configuration string given below the Installation Options
  section, and copy it to your clipboard.

* In your Drupal account page, paste the Configuration String copied in the
  space provided under PersonalTube tab in 'Site Configuration' and save the
  changes.

* If you wish to make changes to your widget video content, colors or
  appearance, you can login in to your PersonalTube account again, make your
  desired changes, save them, and view your web page again to view the modified
  results.


-- CONTACT --

* drupal@personaltube.com
